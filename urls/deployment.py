from flask import Blueprint, request, render_template, current_app
import subprocess, os
deployment = Blueprint('deployment', __name__, url_prefix='/deployment')


@deployment.route("/submission", methods=['POST'])
def deployment_submission():
    if request.method == 'POST':
        form_dict = dict(request.form)
        edit_server = bool(form_dict.get('edit_server')) if form_dict.get('edit_server') else False
        edit_task = bool(form_dict.get('edit_task')) if form_dict.get('edit_task') else False
        deploy_task = bool(form_dict.get('deploy_task')) if form_dict.get('deploy_task') else True
        enable_tabs = {'edit_server': edit_server, 'edit_task': edit_task, 'deploy_task': deploy_task}
        print(form_dict)
        return render_template('deployment/action_details.html', **enable_tabs)


@deployment.route("/action_details", methods=['POST'])
def action():
    if request.method == 'POST':
        form_dict = dict(request.form)
        host_details = form_dict.get('host_details')
        task_details = form_dict.get('task_details')
        group_name = form_dict.get('group_name')
        print(form_dict)
        if host_details:
            with open(os.path.realpath(__file__).replace('deployment.py', "../utils/hosts"), 'w') as f:
                f.write(host_details)
        if task_details:
            task_details = task_details.split('\n')
        command = '/usr/local/bin/ansible-playbook -i /Users/selvat/PycharmProjects/hunt-look/utils/hosts /Users/selvat/PycharmProjects/hunt-look/utils/basic_task.yml'
        proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, _ = proc.communicate()
        proc.wait()
        out = out.decode("utf-8")
        if out != "":
            out = out.rstrip('\n')
        print(out)
